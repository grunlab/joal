#!/bin/sh

if [ ! "$(ls -A /data)" ]; then
  mkdir -p /data/torrents/archived
  cp /opt/joal/config.json /data/.
else
  rm -rf /data/clients
fi

cp -r /opt/joal/clients /data/.

java -jar /opt/joal/joal.jar \
     --joal-conf=/data \
     --spring.main.web-environment=true \
     --joal.ui.path.prefix=joal \
     --joal.ui.secret-token=joal \
     --server.port=8080

