# GrunLab Joal

Joal non-root container image and deployment on Kubernetes.

Docs:
- https://docs.grunlab.net/images/joal.md
- https://docs.grunlab.net/apps/joal.md

Base image: [grunlab/base-image/debian:12][base-image]

Format: docker

Supported architecture(s):
- arm64

[base-image]: <https://gitlab.com/grunlab/base-image>